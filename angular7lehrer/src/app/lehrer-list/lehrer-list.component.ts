import { Observable } from "rxjs";
import { LehrerService } from "./../lehrer.service";
import { Lehrer } from "./../lehrer";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: 'app-lehrer-list',
  templateUrl: './lehrer-list.component.html',
  styleUrls: ['./lehrer-list.component.scss']
})
export class LehrerListComponent implements OnInit {

  lehrer: Observable<Lehrer[]>;

  constructor(private lehrerService: LehrerService) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.lehrer = this.lehrerService.getLehrerList();
    //console.log(this.lehrer);
  }

  deleteLehrer(id: string) {
  this.lehrerService.deleteLehrer(id)
    .subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error));
    }

}
