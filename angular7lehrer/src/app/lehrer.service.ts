import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LehrerService {

  private baseUrl = 'http://localhost:9080/RestVorlage/api/teachers';

  constructor(private http: HttpClient) { }

  getLehrer(id: string): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createLehrer(lehrer: Object): Observable<Object> {
    console.log(lehrer);
    return this.http.post(`${this.baseUrl}`, lehrer);
  }

  updateLehrer(id: string, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteLehrer(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getLehrerList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
