import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateLehrerComponent } from './create-lehrer/create-lehrer.component';
import { LehrerDetailsComponent } from './lehrer-details/lehrer-details.component';
import { LehrerListComponent } from './lehrer-list/lehrer-list.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CreateLehrerComponent,
    LehrerDetailsComponent,
    LehrerListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
