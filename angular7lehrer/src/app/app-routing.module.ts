import { LehrerDetailsComponent } from './lehrer-details/lehrer-details.component';
import { CreateLehrerComponent } from './create-lehrer/create-lehrer.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LehrerListComponent } from './lehrer-list/lehrer-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'lehrer', pathMatch: 'full' },
  { path: 'lehrer', component: LehrerListComponent },
  { path: 'add', component: CreateLehrerComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
