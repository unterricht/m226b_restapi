import { TestBed } from '@angular/core/testing';

import { LehrerService } from './lehrer.service';

describe('LehrerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LehrerService = TestBed.get(LehrerService);
    expect(service).toBeTruthy();
  });
});
