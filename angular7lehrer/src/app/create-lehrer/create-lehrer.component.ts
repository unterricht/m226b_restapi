import { Component, OnInit } from '@angular/core';
import { LehrerService } from './../lehrer.service';
import { Lehrer } from './../lehrer';

@Component({
  selector: 'app-create-lehrer',
  templateUrl: './create-lehrer.component.html',
  styleUrls: ['./create-lehrer.component.scss']
})
export class CreateLehrerComponent implements OnInit {

  lehrer: Lehrer = new Lehrer();
  submitted = false;

  constructor(private lehrerService: LehrerService) { }

  ngOnInit() {
  }

  newLehrer(): void {
    this.submitted = false;
    this.lehrer = new Lehrer();
  }

  save() {
    this.lehrerService.createLehrer(this.lehrer)
      .subscribe(data => console.log(data), error => console.log(error));
    this.lehrer = new Lehrer();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
