import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLehrerComponent } from './create-lehrer.component';

describe('CreateLehrerComponent', () => {
  let component: CreateLehrerComponent;
  let fixture: ComponentFixture<CreateLehrerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLehrerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLehrerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
