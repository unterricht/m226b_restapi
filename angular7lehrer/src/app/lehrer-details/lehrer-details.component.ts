import { Component, OnInit } from '@angular/core';
import { Lehrer } from './../lehrer';
import { LehrerService } from '../lehrer.service';
import { LehrerListComponent } from '../lehrer-list/lehrer-list.component';

@Component({
  selector: 'app-lehrer-details',
  templateUrl: './lehrer-details.component.html',
  styleUrls: ['./lehrer-details.component.scss']
})
export class LehrerDetailsComponent implements OnInit {

  //@Input() lehrer: Lehrer;

  constructor(private lehrerService: LehrerService, private listComponent: LehrerListComponent) { }

  ngOnInit() {
  }

}
