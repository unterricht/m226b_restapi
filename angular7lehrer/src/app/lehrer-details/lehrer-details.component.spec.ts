import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LehrerDetailsComponent } from './lehrer-details.component';

describe('LehrerDetailsComponent', () => {
  let component: LehrerDetailsComponent;
  let fixture: ComponentFixture<LehrerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LehrerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LehrerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
